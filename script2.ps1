#SI,ALORS,SINON
#Structure de d�cision simple
#Si on a 18ans alors videur dit "C'est bon entrez" sinon "Ca va pas �tre possible"
#Si on a pas 18ans mais que l'on a Backchich (p�t de vin) alors "Ca ira pour cette fois"
$age = 22
$Backchich = 0
$porte_des_baskets = $true
if ($age -gt 18) {
write-host "C'est bon, entrez"
}elseif($Backchich -gt 100) {
write-host "Ca ira pour cette fois"
}else{
write-host "Ca va pas �tre possible"
}

#ca depends des cas...
#Structure de d�cision multi-branche
switch ($true) {
($porte_des_baskets){
write-host "Nope" ; break }
($age -lt 18 -and $backchich -gt 100) {
write-host "Ok" }
($age -ge 18 ){
write-host "Ok" }
Default { write-host "Ca va pas �tre possible" }
}
#[int] est un tag qui permet ensuite que n'importe quel valeur qui sera rentr� sera un chiffre.
[int]$ma_chaine = Read-Host "age ?"
#$ma_chaine | gm 

#Cr�e une boucle allant de 1 � 10 
for ($i = 0; $i -lt 10; $i++){
write-host $i
}

#Boucle sur tous les objets d'une liste
$contenu = gc .\script1.ps1
#foreach = pour chaque, donc pour chaque ligne dans le contenu script1 afficher le contenu de la ligne
foreach ($ligne in $contenu) {
write-host $ligne
}
#Et si cela plante ?
try {
gc "fichier_qui_existe_pas.xml"
}
catch {
write-host "Pas de fichier !"
}

#LES SOUS-PROGRAMMES
#On cr�e un outil qui va faire les taches r�currentes
#r�gion cr�ation de la fonction

cls
$chemin_du_fichier = "C:\Users\thoma\Desktop\SCRIPT\held.raphael\banana.txt"

function afficher_le_contenu{
param($chemin_du_fichier)
$contenu = gc $chemin_du_fichier
foreach ($item in $contenu) {
write-host $item
}
}

#endregion

#Utilisation de la fonction
afficher_le_contenu($chemin_du_fichier)
#endregion

#Utilisation de la fonction
afficher_le_contenu($chemin_du_fichier)
